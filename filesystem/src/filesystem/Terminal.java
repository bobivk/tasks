package filesystem;

import java.util.Map.Entry;
import java.util.Scanner;

public class Terminal {
	Directory currentDirectory;
	private String path;
	Scanner scanner;

	public Terminal(Scanner scanner) {
		this.path = "C:/";
		this.scanner = scanner;
	}
	//touch works for text files only!!
	public void touch(String fileName) {
		// put file in dir hashmap
		currentDirectory.getContents().put(currentDirectory.getName()
													, new TextFile(fileName, this.getPath()));
		// set file parent to current dir
		currentDirectory.getContents().get(fileName).setParent(currentDirectory);
	}
	
	public void mkdir(String directoryName) {
		// add to dir hashmap
		currentDirectory.getContents().
			put(currentDirectory.getName(), new Directory(directoryName, this.getPath()));
		// set new dir's parent to currentdir
		currentDirectory.getContents().get(currentDirectory.getName()).
			setParent(currentDirectory);
		currentDirectory.getContents().get(currentDirectory.getName()).setPath(this.getPath());
		
	}
	public void ls() {
		for(String i: currentDirectory.getContents().keySet()) {
			System.out.println(currentDirectory.getContents().get(i));
		}
	}
	public void cd(Directory destinationDir) {
		this.currentDirectory = destinationDir;
		//update path
		path += destinationDir.getName();
		
	}
	public void mv(File currentFile, Directory destinationDir) {
		currentFile.setParent(destinationDir);
		destinationDir.addFile(currentFile);
	}
	public void rm(File currentFile) {
		currentDirectory.removeFile(currentFile);
	}
	public void rename(File currentFile, String newName) {
		currentFile.setName(newName);
	}
	public void pwd() {
		System.out.println(path);
	}
	public void cat(filesystem.TextFile file) {
		for(int i = 0; i < file.getTextContent().size(); i++) {
			System.out.print(file.getTextContent().get(i));
		}
	}
	public void vim(filesystem.TextFile file) {
		file.vim(scanner);
	}

	//copy and paste file
	public void cp(File file, Directory destinationDir) {
		if (file.getClass().equals(TextFile.class)){
			TextFile copiedFile = new TextFile(file.getName(), file.getPath());
			copiedFile.setParent(destinationDir);
			copiedFile.setTextContent(file.getTextContent());
			destinationDir.addFile(copiedFile);
		} else if(file.getClass().equals(Directory.class)){
			Directory copiedDir = new Directory(file.getName(), file.getPath());
			copiedDir.setParent(destinationDir);
			copiedDir.setContent(file.getFileContent());
			destinationDir.addFile(copiedDir);
		} else {
			System.out.println("The file is not a Directory or TextFile");
		}
	}
	//copy the text from one file to another NOTE: OVERWRITES THE SECOND FILE'S INITIAL TEXT
	public void cp(TextFile file1, TextFile file2){
		file2.setTextContent(file1.getTextContent());
	}
	public void cp(TextFile file1, String file2Name){
		TextFile file2 = new TextFile(file2Name, file1.getPath());
		file2.setTextContent(file1.getTextContent());
	}
	
	public void locate(File file) {
		//check if file is in current dir
		if(currentDirectory.getContents().containsKey(file.getName())) {
			currentDirectory.getContents().get(file.getName());
		} else {
			//loop through inner directories
			for(Entry<String, File> entry : currentDirectory.getContents().entrySet()) {
				// TODO: iterate through hashmap to locate file
			}
		}
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
